from flask import jsonify

from controller import blueprint
import json
import datetime
from model.user_profile import User, UserSchema, Phone
from model import DBSession

# import db session
db = DBSession()


@blueprint.route('/hello', methods=['GET'])
def say_hello():
    return 'Hello_world~~~'


@blueprint.route('/demoReturnJson', methods=['GET'])
def demo_return_json():
    return_data = {
        'name': 'tim',
        'date': str(datetime.datetime.now())
    }

    return json.dumps(return_data)


@blueprint.route('/getAllUser', methods=['GET'])
def get_all_User():
    # query all user from db
    all_user = db.query(User).all()

    # convert object to dict
    dump_data = UserSchema().dump(all_user, many=True).data

    return json.dumps(dump_data)


from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, BooleanField, FormField
from wtforms.validators import DataRequired, Email, ValidationError


# define input form data object
class PhoneForm(FlaskForm):
    id = IntegerField('id')
    user_id = IntegerField('user_id')
    number = StringField('number', validators=[DataRequired()])
    remark = StringField('remark')


# define input form data object
class UserForm(FlaskForm):
    id = IntegerField('id')
    name = StringField('name', validators=[DataRequired()])
    gender = BooleanField('gg', validators=[DataRequired()])

    email = StringField('email', validators=[DataRequired(), Email()])

    # relational form data object
    phone = FormField(PhoneForm)

    # custom validator for email validation
    def validate_email(self, field):
        user = db.query(User).filter(User.email == field.data).all()

        if len(user) > 0:
            raise ValidationError('this mail is already used')


@blueprint.route('/addNewUser', methods=['POST'])
def add_new_user():
    """
        input post json data:
        {
            "name":"tim_01",
            "gender":1,
            "email_0":"user email",
            "email":"tim@email.com",
            "phone-number":"0955-555-555",
            "phone-remark":"test phone number"
        }
    """
    # get data from form.data, add create UserForm object
    user_form = UserForm()
    # validate input data
    if not user_form.validate():
        return json.dumps(user_form.errors), 400

    # create Phone object
    phone = Phone()
    phone.number = user_form.phone.number.data
    phone.remark = user_form.phone.remark.data

    # create User object
    new_user = User()
    db.add(new_user)
    db.flush()

    # assign data into user object
    new_user.name = user_form.name.data
    new_user. gender = user_form.gender.data
    new_user.email = user_form.email.data
    new_user.phones = [phone]

    phone.user_id = new_user.id

    db.commit()

    return_value = dict()
    return_value['message'] = "success"

    return json.dumps(return_value), 200
