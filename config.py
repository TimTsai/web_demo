# config file
DEBUG = True
WTF_CSRF_ENABLED = False
SECRET_KEY = 'any secret string'
WTF_CSRF_SECRET_KEY = 'a csrf secret key'
DB_CONNECTION_STRING = 'mysql+mysqlconnector://root:123456789@localhost:3306/python_web_demo'