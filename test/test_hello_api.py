import requests
import unittest
import json


class TestHelloApi(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.serverUrl = 'http://127.0.0.1:5000'

    def test_say_hello(self):

        url = '{}{}'.format(self.serverUrl, '/hello')
        result = requests.get(url)
        assert result.status_code == 200
        assert result.text == 'Hello_world~~~'

    def test_demo_return_json(self):
        url = '{}{}'.format(self.serverUrl, '/demoReturnJson')
        result = requests.get(url)
        assert result.status_code == 200
        result_json = json.loads(result.text)
        assert result_json['name'] == 'tim'

    def test_get_all_User(self):
        url = '{}{}'.format(self.serverUrl, '/getAllUser')
        
