from sqlalchemy import Column, String, Integer, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from . import Base


# sqlalchemy orm object
class User(Base):
    __tablename__ = 'user'
    id = Column('id', Integer, primary_key=True)
    name = Column('name', String)
    gender = Column(Boolean)
    email = Column('email', String)
    # relational column
    phones = relationship('Phone', order_by='Phone.id', backref='user')
    addresses = relationship('Address', order_by='Address.id', backref='user')

    def __init__(self, name='', gender=False, email='', phones=list(), addresses=list()):
        self.name = name
        self.gender = gender
        self.email = email
        self.phones = phones
        self.addresses = addresses


class Phone(Base):
    __tablename__ = 'phone'
    id = Column('id', Integer, primary_key=True)
    user_id = Column('user_id', Integer, ForeignKey('user.id'))
    number = Column('number', String)
    remark = Column('remark', String)

    # def __init__(self, user_id, number, remark):
    #     self.user_id = user_id
    #     self.number = number
    #     self.remark = remark


class Address(Base):
    __tablename__ = 'address'
    id = Column('id', Integer, primary_key=True)
    user_id = Column('user_id', Integer, ForeignKey('user.id'))
    address = Column('address', String)
    remark = Column('remark', String)

    # def __init__(self, user_id, address, remark):
    #     self.user_id = user_id
    #     self.address = address
    #     self.remark = remark


# declared output json schema
from marshmallow import Schema, fields


class PhoneSchema(Schema):
    id = fields.Int()
    number = fields.Str()
    remark = fields.Str()


class AddressSchema(Schema):
    id = fields.Int()
    address = fields.Str()
    remark = fields.Str()


class UserSchema(Schema):
    id = fields.Int()
    name = fields.Str()
    gender = fields.Bool()

    # add relational object list
    phones = fields.Nested(PhoneSchema, many=True)
    addresses = fields.Nested(AddressSchema, many=True)
