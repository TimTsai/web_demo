from flask import Flask
from controller import blueprint
import config

app = Flask(__name__)
app.config.from_object(config)

app.register_blueprint(blueprint)


if __name__ == '__main__':
    app.run()
